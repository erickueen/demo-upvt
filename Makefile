test:
	docker-compose run --rm spring sh -c "/app/src/gradlew test"
run:
	docker-compose run --service-ports spring sh -c "/app/src/gradlew bootRun"
