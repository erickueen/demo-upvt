# Ejemplo de aplicación con Docker
Esta app tiene como objetivo mostrar una implementación sencilla de Docker y docker-compose.

La app desplegada se encuentra en: [demo-upvt.vps.erickueen.dev](https://demo-upvt.vps.erickueen.dev/)

## Componentes de la aplicación:
  - Gradle como gestor de dependencias
  - Spring Boot como framework
  - Jupiter para unit testing
  - Postgresql como base de datos
  - Docker para generar las imágenes
  - Make para alias de comandos

## Requisitos para ejecutar la aplicación
  - Docker
  - Make (Opcional)

## Comandos de la aplicación:
  - make test: Ejecuta las pruebas unitarias de la app.
  - make run: Levanta la aplicación en modo desarrollo.
  - make build: Genera una imagen productiva del proyecto.
