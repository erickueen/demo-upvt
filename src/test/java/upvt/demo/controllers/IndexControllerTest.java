package upvt.demo.controllers;

import org.junit.jupiter.api.Test;
// El metodo assertThat nos permite probar algo y asegurarnos de que el resultado sea verdadero para que el test
// Se tome como un test valido.
import static org.assertj.core.api.Assertions.assertThat;

import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

//Importamos paquetes locales
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import upvt.demo.controllers.IndexController;

// El paquete MockMvc nos permite hacer pruebas de endpoints del servidor sin necesidad de levantar un servidor de verdad.
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import upvt.demo.services.LinkService;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@AutoConfigureMockMvc
class IndexControllerTest {

  @Autowired
  IndexController controller;

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private LinkService linkService;

  @Test
  public void controllerClassExists() throws Exception {
    assertThat(controller).isNotNull();
  }

  @Test
  public void shouldRenderIndexPage() throws Exception {
    Mockito.when(linkService.findAll()).thenReturn(null);
    this.mockMvc
    .perform(get("/"))
    .andDo(print())
    .andExpect(status()
    .isOk())
    .andExpect(content()
    .string(containsString("Tecnologías útiles para desarrolladores")));
  }
}
