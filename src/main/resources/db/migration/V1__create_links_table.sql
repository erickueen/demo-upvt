create table links (
    id SERIAL PRIMARY KEY,
    name varchar(255),
    url varchar(255)
)
