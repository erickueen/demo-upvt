package upvt.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

import org.springframework.ui.Model;
import upvt.demo.services.ILinkService;
import upvt.demo.models.Link;

/**
* Controlador principal de la app.
*/
@Controller
class IndexController {

  @Autowired
  private ILinkService linkService;


  /**
  * Metodo Index.
  * Obtiene todos los Links de la base de datos y los mapea a el template index de Thymeleaf
  */
  @GetMapping("/")
  public String index(Model model) {
    var links = (List<Link>) linkService.findAll();

    model.addAttribute("links", links);

    return "index";
  }
}
