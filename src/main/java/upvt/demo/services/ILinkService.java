package upvt.demo.services;

import upvt.demo.models.Link;
import java.util.List;

/**
* Interfaz del servicio para obtener datos de los Links en la base de datos
*/
public interface ILinkService {
  List<Link> findAll();
}
