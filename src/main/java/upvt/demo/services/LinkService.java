package upvt.demo.services;

import upvt.demo.services.ILinkService;
import java.util.List;

import upvt.demo.models.Link;
import upvt.demo.repositories.LinkRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* Servicio que contiene los métodos para obtener datos de los Links en la base de datos.
* Implementa la interfaz ILinkService y hace uso del repositorio correspondiente.
*/
@Service
public class LinkService implements ILinkService {
  @Autowired
  private LinkRepository repo;

  /**
  * Busca todos los Links en la base de datos y los regresa como una Lista de Java
  */
  @Override
  public List<Link> findAll() {
    var links = (List<Link>) repo.findAll();
    return links;
  }
}
