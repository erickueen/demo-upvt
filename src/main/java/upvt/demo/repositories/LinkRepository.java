package upvt.demo.repositories;

import upvt.demo.models.Link;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repositorio para Links, hace uso de la interfaz CrudRepository
 * de spring boot para heredar métodos básicos de un CRUD.
 */
@Repository
public interface LinkRepository extends CrudRepository<Link, Long> {
}
