FROM gradle:6.8.3-jdk11
LABEL mantainer="Erick Reyna <hello@erickueen.dev>"

COPY ./ /app/src
WORKDIR /app/src
RUN gradle assemble
RUN ls /app/src/build/libs

FROM openjdk
COPY --from=0 /app/src/build/libs/demo-0.0.1-SNAPSHOT.jar /rel/demo.jar
WORKDIR /rel
CMD java -jar /rel/demo.jar
